// house, livingRoom, mainBedRoom, , garden

function printME(object) {
    console.log(JSON.stringify(object));
}

function printMeWithContext() {
    console.log(JSON.stringify(this));
}

var House = function () {
    this.livingRoom = {
        chairsNumber: 4
    }

    this.levels = 2;

    this.printME = function () {
        console.log(JSON.stringify(this));
    }
}

const prettyHouse = new House();

prettyHouse.printME.call({ name: 'I am a Building!'});
printME(prettyHouse);
printMeWithContext.call(prettyHouse);

function open() {
    console.log('open');
    console.log(this);
    var a = 'x';

    function doOpen() {
        console.log('do Open');
        console.log(this);
    }

    doOpen();
}

open();

open.call({});

var o = new open();

throw 'e';

console.log('COOL!');

var coolWindow = {
    buttons: ['btn1', 'btn2'],
    main(){
        var x;
        this.y = 3;

        console.log('main');

        function mainWork(){
            console.log('main work');
            console.log(this);
        }

        mainWork(this);
    }
}

console.log(coolWindow);
coolWindow.main();

/*

1. Lexical context
1.1 Who called it?
2. call, apply, bind
3. new
4. Events Case

It's because, until ECMAscript 262 edition 5, there was a big confusion if people 
who where using the  constructor pattern, forgot to use the new keyword. 
If you forgot to use new when calling a constructor function in ES3, 
this referenced the global object (window in a browser) and you would clobber the global object 
with variables. That was terrible behavior and so people at ECMA decided, just to set this to undefined

*/