'use strict';

this.printMe = function () {
    console.log(this);

    function printMeAdvanced(params) {
        console.log(this);
    }

    printMeAdvanced.call({ n: 'peter'});
}

this.printMe();

var House = function (name) {
    this.name = name;
    this.printMe = function() {
        console.log('I am a Structure. My name is: ' + this.name);
    }
};

let prettyHouse = new House('Pretty');
prettyHouse.printMe();

let building = {
    name: 'Cool Building'
}
prettyHouse.printMe.call(building);
prettyHouse.printMe.bind(building)();

