let sourceCode = $('#sourceCode').html();

console.log(sourceCode);

let monitorizedExecution = {
    executions: [],
    add(fn, requiresTimeout, timeoutInterval) {
        this.executions.push({ fn, requiresTimeout, timeoutInterval });
    },
    run() {
        this.executions.forEach(executionInstruction => {
            if (executionInstruction.requiresTimeout) {
                setTimeout(function () {
                    executionInstruction.fn();
                    console.log('completed!');
                }, executionInstruction.timeoutInterval);
            } else {
                executionInstruction.fn();
                console.log('completed!');
            }
        });
    }
}

monitorizedExecution.add(function () {
    console.log('I am not asynchronus');
}, false);

monitorizedExecution.add(function () {
    console.log('I am the first message!');
}, true, 1000);

monitorizedExecution.add(function () {
    console.log('I am the second message!');
}, true, 1000);

monitorizedExecution.add(function () {
    console.log('I am the third message!');
}, true, 1000);

monitorizedExecution.run();
