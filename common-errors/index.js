// Common Errors

// When working with conditionals and iterations: always set the { }, no matter the number of lines inside it.
// Even though Javascript parser automatically injects ;, we should know the cases when it causes issues
// Type convertions http://prntscr.com/nbg6j8
// Object convertions http://prntscr.com/nbg8ht
// Implicit Type convertions http://prntscr.com/nbgbcr
// Coercion http://prntscr.com/nbgblq
// Var declarations: Remember to separate vars by comma http://prntscr.com/nbgcol
// Primitives and Objects http://prntscr.com/nbgg7f

let bmw1 = new Car('BMW', 2010, 100);
let mazda2 = new Car('Mazda 2', 2012, 50);
let bmw2 = new Car('BMW', 2011, 80);
let carRental = new Rental(car);

let taxesCalculation = {
    getHourlyRate: function() {
        return this.hourlyRate*0.16;
    }
}

// You should add all necessary code to get the desired results.
// taxesCalculation Object must be used to calculate each car taxes.
let taxesCalculation = {
    getHourlyRate: function() {
        return this.hourlyRate*0.16;
    }
}

carRental.addCar(bmw1); // 'BMW', 2010, 100
carRental.addCar(mazda2); // 'Mazda 2', 2012, 50
carRental.addCar(bmw2); // 'BMW', 2011, 80
console.log(carRental.getAverageAgeModel()); // 2011
console.log(carRental.getFavoriteBrand()); // BMW
console.log(carRental.getCheapestCar()); // { Car('Mazda 2', 2012, 50); }
carRental.rentCar(bmw2, 24); // Rent bmw2 for 24 hours
console.log(carRental.getAvailableCarsCount()); // 2
console.log(carRental.getTotalRentTaxes()); // Taxes for all rents: 307.2

luxuryCarRental.addCars(bmw1, bmw2, mazda2);
luxuryCarRental.rentCar(bmw2, 24); // Rent bmw2 for 24 hours
console.log(luxuryCarRental.getTotalRentTaxes()); // Taxes for all rents: 337.92 -> Hourly Rate is increased by 10% on Luxury